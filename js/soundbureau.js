

// хранилище состояния UI

var Soundbureau = {};


// основная точка входа скриптов

$(document).on("ready", function() {

    // задаём начальную позицию табов
    $(".tabs").each(function() {
    	setTab($(this).find("a[data-toggle-tab]").eq(0));
    });

    $(".tabs-colors").each(function() {
    	setTab($(this).find("a[data-toggle-tab]").eq(0));
    });

    // сворачиваем табы цветов
    $("#promo-colors").slideToggle(function() {
    	refreshPageSize();
    });

    // сворачиваем аккордеон
    $(".accordeon-content").each(function() {
    	if($(this).parents(".accordeon.expand-always").length < 1) {
    		$(this).addClass("cut");
    	}    	
    });

    // инциализация главной карусели
    var mainPromo = $("#main-promo");
    if(mainPromo.length > 0) {
    	$(window).on("load", function() {
    		syncMainPromo(mainPromo.data("fotorama"));
    	});
    	mainPromo.on("fotorama:show", function(event) {
    		syncMainPromo(mainPromo.data("fotorama"));
    	});
    }

    // настраиваем фоторамы рабочих страниц
    $(".main-illustration-nav").each(function() {
    	var nav = $(this);
    	var label = nav.find(".label");
    	var api = nav.siblings(".main-illustration.fotorama").eq(0).data("fotorama");
    	var total = api.size;
    	label.text((api.activeIndex+1)+"/"+total);
    	window.fff = api;

    	nav.on("click", "a[data-prev]", function() {
    		api.show("<");
    		label.text((api.activeIndex+1) + "/" + total);
    	});

    	nav.on("click", "a[data-next]", function() {
    		api.show(">");
    		label.text((api.activeIndex+1) + "/" + total);
    	});

    	var fotorama = nav.siblings(".main-illustration.fotorama");
    	fotorama.on("fotorama:show", function(event) {
    		console.log(api.activeIndex, event);
    		label.text((api.activeIndex+1) + "/" + total);
    	});
    });

	// настраиваем переключение страниц - карусель owl.carousel
	var firstLoaded = true;

	var pages = $(".pages").eq(0);
	pages.on("changed.owl.carousel", function(e) {
		Soundbureau.currentPage = e.item.index ? e.item.index : 0;
		if(firstLoaded == false) {
			location.href = location.href.split("#")[0] + "#" + $("article.page").eq(Soundbureau.currentPage).attr("data-hash");
		}
		if(firstLoaded) firstLoaded = false;
		refreshAllInterface();
		$("body, html").animate({
			scrollTop: 0
		}, 100);
	});
	pages.owlCarousel({
        items: 1,
        URLhashListener: true,
        autoHeight: true,
        mouseDrag: false,
        touchDrag: false,
        startPosition: 'URLHash'
    });
    $("a[data-page-next]").on("click", function() {
    	if(Soundbureau.currentPage == pages.find(".page").length-1) {
    		pages.trigger("to.owl.carousel", [0])
    	} else {
    		pages.trigger("next.owl.carousel");
    	}
    	refreshAllInterface(true);
    });
    $("a[data-page-prev]").on("click", function() {
    	if(Soundbureau.currentPage == 0) {
    		pages.trigger("to.owl.carousel", [pages.find(".page").length-1])
    	} else {
    		pages.trigger("prev.owl.carousel");
    	}
    	refreshAllInterface(true);
    });
    $(window).on("load",function() {
    	refreshPageSize();
    });

    // Запускаем анимации
    $("*[data-onload-animation-order]").each(function() {
    	var order = parseInt($(this).attr("data-onload-animation-order"));
    	if(!order) order = 1;
    	var element = $(this);

    	setTimeout(function() {
    		element.addClass("start");
    	}, (order-1)*250)
    });

    // прелоадеры
    $("img").each(function() {
    	$(this).parent().addClass("preloader");
    });
    $("*[style*=background-image]").each(function() {
    	$(this).addClass("preloader");
		var t = $(this);
    	
    	var img = $("<img style='position: fixed; z-index: -1; visibility: hidden;' src='" + $(this).attr("style").split("url(")[1].split(")")[0] + "'>");
    	$(this).append(img);
    	img.on("load", function() {
    		t.addClass("loaded");
    		console.log("double", img.get(0));
			setTimeout(function() {
				t.removeClass("preloader loaded");
			}, 500);
    	});
    });
    setTimeout(function() {
    	$(".preloader").removeClass("preloader");
    },2500);

	$("img").on("load", function() {
		$(this).parent().addClass("loaded");
		var t = $(this).parent();
		// console.log(t.get(0));
		setTimeout(function() {
			t.removeClass("preloader loaded");
		}, 500);
	});
});




// карусель на главной - стрелки и синхронизация заголовков и ссылок

$(document).on("click", "a[data-main-promo-prev]", function() {
	var api = $("#main-promo").data("fotorama");
	api.show("<");
});

$(document).on("click", "a[data-main-promo-next]", function() {
	var api = $("#main-promo").data("fotorama");
	api.show(">");
});

function syncMainPromo(api) {
	if(!api) {
		api = $("#main-promo").data("fotorama");
	}
	if(api.activeFrame) {
		var header = $(api.activeFrame.html).find("header");
		var control = $("#main-promo-control-stripe").find("header");
		var title = header.find("h1").text().replace(/(\r\n|\n|\r)/gm,"");
		var href = header.find("a").eq(-1).attr("href");
		control.find("h3").text(title);
		control.find("a").attr("href", href);
	}
}


// ставим актуальное выделение в меню верхнем

function refreshAllInterface(hash) {
	// выделение текущего пункта в подменю
	$("ul#site-menu > li.active > ul.submenu > li").each(function(index) {
		if(index == Soundbureau.currentPage) {
			$(this).addClass("active");
		} else {
			$(this).removeClass("active");
		}
	});
}


// чтоб размер страницы поменять на актуальный в карусели страниц

function refreshPageSize(height) {
	var h = $(".owl-item.active").height();
	$(".owl-item.active").parents(".owl-height").height(h + (height ? height : 0));
}


// виджет аккордеон

$(window).on("load", function() {
	// троеточие и обрезка текста
    $(".accordeon-content").each(function() {
    	if($(this).parents(".accordeon.expand-always").length < 1) {
    		$(this).dotdotdot();
    	}
    });
});

$(document).on("click", ".accordeon a[data-toggle-more]", function() {
	var content = $(this).parents("li").eq(0).find(".accordeon-content").eq(0);
	if(!content.hasClass("cut")) {
		// cut
		content.css("height", "");
		content.addClass("cut");
		setTimeout(function() {
			content.dotdotdot();
		},150);	
		$(this).text("Читать больше");
	} else {
		content.children().remove();
		content.append(content.triggerHandler("originalContent"));
		// uncut
		var clone = content.clone();
		content.parent().append(clone);
		clone.css({
			"-webkit-transition-duration": "0s",
			"-moz-transition-duration": "0s",
			"transition-duration": "0s"
		});
		clone.removeClass("cut");
		var h = 0;
		setTimeout(function() {
			h = clone.height();
			content.height(h)
			content.removeClass("cut");
			clone.remove();
			setTimeout(function() {
				content.get(0).style.display = "none";
				content.get(0).offsetHeight;
				content.get(0).style.display = "block";
			},200);
			refreshPageSize(h);
		},1);

		$(this).text("Скрыть");
	}
	setTimeout(function() {
		refreshPageSize();
	},500);
});


// виджет табы

function setTab(index, group) {
	// если нам дали элемент
	if(index.css) {
		// либо это ссылка на таб
		if(index.attr("href")) {
			var links = index.siblings("a[data-toggle-tab]");
			var target = $(index.attr("href"));
			if(target.length > 0) {
				var group = $("*[data-tab-group='" + target.attr("data-tab-group") + "']");
				group.addClass("inactive");
				target.removeClass("inactive");
				index.addClass("active");
				links.removeClass("active");
			}
		}
		// либо сам таб
		else if(index.attr("data-tab-group")) {
			console.log("tab");
		}
		console.log("nothing");
	} 
	// если номер, значит это id
	else if(typeof(index) == "number") {
		console.log("number");
	}
	refreshPageSize();
}

$(document).on("click", "a[data-toggle-tab]", function(e) {
	setTab($(this));
	var te = $($(this).attr("data-title-target"));
	if(te.length > 0) {
		te.html($(this).html());
	}
	e.preventDefault();
});


// вкл/выкл контакты

$(document).on("click", "*[data-toggle='#contacts']", function() {
	var heightSmall = 84;
	var heightBig = $("#contacts").outerHeight() + heightSmall;
	var wrapper = $(".overheader-animation-placeholder");
	wrapper.toggleClass("active");
	$("body").toggleClass("popup");
	if(wrapper.hasClass("active")) {
		wrapper.height(heightBig);
	} else {
		wrapper.height(heightSmall);
	}
	wrapper.find(".overlay").fadeToggle();
});


// вкл/выкл табы цветов

$(document).on("click", "*[data-toggle='#promo-colors']", function() {
	var self = $(this);
	$("#promo-colors").slideToggle(function() {
		refreshPageSize();
	});
	setTimeout(function() {
		self.toggleClass("active");
	},200);
});


// превращаем картинки .svg в инлайновые теги svg

function parseSVGimages(images) {
	if(!images) {
		images = $("img[src$='.svg']")
	}
	images.each(function() {
		var img = $(this);
		$.get(img.attr("src"), function(result) {
			var svg = $(result).find("svg");
			svg.attr("class", img.attr("class"));
			img.after(svg);
			img.remove();
		})
	});
}